const app = (module.exports = require('express')());
const { linkList, linkDetail, linkCreate } = require('../controllers').link;

app.get('/', linkList);
app.post('/create', linkCreate);
app.get('/:linkId', linkDetail);
