const app = (module.exports = require('express')());
const {
  linkDetailView,
  linkValidateCreate,
  linkCreateView,
  linkListView
} = require('../controllers').linkView;

app.get('/', linkListView);
app.post('/create', linkValidateCreate);
app.get('/create', linkCreateView);
app.get('/:linkId', linkDetailView);
