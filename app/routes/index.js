const app = (module.exports = require('express')());

app.get('/', async (req, res, next) => {
  res.redirect('/link');
});

app.use('/link', require('./linkView'));

app.use('/api/link', require('./link'));
