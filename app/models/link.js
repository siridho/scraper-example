'use strict';
module.exports = (sequelize, DataTypes) => {
  const Link = sequelize.define(
    'Link',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER.UNSIGNED
      },
      link: {
        allowNull: false,
        type: DataTypes.TEXT
      },
      productName: {
        field: 'product_name',
        type: DataTypes.STRING,
        allowNull: true
      },
      productPrice: {
        field: 'product_price',
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull: true
      },
      productDescription: {
        field: 'product_description',
        type: DataTypes.TEXT,
        allowNull: true
      },
      createdAt: {
        field: 'created_at',
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
      },
      updatedAt: {
        field: 'updated_at',
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
      }
    },
    {
      tableName: 'links'
    }
  );

  Link.associate = models => {
    Link.hasMany(models.ProductImage, {
      foreignKey: 'linkId',
      as: 'images'
    });
  };

  return Link;
};
