'use strict';
module.exports = (sequelize, DataTypes) => {
  const ProductImage = sequelize.define(
    'ProductImage',
    {
      linkId: {
        field: 'link_id',
        allowNull: false,
        primaryKey: true,
        type: DataTypes.INTEGER.UNSIGNED
      },
      imageUrl: {
        field: 'image_url',
        allowNull: false,
        primaryKey: true,
        type: DataTypes.TEXT
      }
    },
    {
      timestamps: false,
      tableName: 'product_images'
    }
  );

  return ProductImage;
};
