const axios = require('axios');
const _ = require('lodash');
const cheerio = require('cheerio');

const accessThisUrl = async input => {
  return await axios(input)
    .then(response => {
      const { data } = response.data;
      // console.log(response.data);
      return { data, errors: null, status: 'success' };
    })
    .catch(function(e) {
      return {
        errors: e,
        data: null,
        status: 'error'
      };
    });
};
const scraperThisUrl = async input => {
  let images = [];
  let productPrice = 0;
  let productName,
    productDescription = '';
  return await axios
    .get(input.link)
    .then(response => {
      let $ = cheerio.load(response.data);

      productName = $('span.base').text();

      productPrice = parseInt(
        $('span.price-container > span.price-wrapper')
          .first()
          .attr('data-price-amount'),
        10
      );

      productDescription = $('div#description').text();

      let str = '';
      $('script:not([src])').each((i, item) => {
        if (
          $(item).attr('type') === 'text/x-magento-init' &&
          $(item).get([0]).children[0].data
        ) {
          str = $(item).get([0]).children[0].data;
          var dataScript = JSON.parse(str);
          if (
            dataScript['[data-gallery-role=gallery-placeholder]'] &&
            dataScript['[data-gallery-role=gallery-placeholder]'][
              'mage/gallery/gallery'
            ]
          ) {
            const arrData =
              dataScript['[data-gallery-role=gallery-placeholder]'][
                'mage/gallery/gallery'
              ].data;

            images = arrData.map(x => {
              return x.img;
            });
          }
        }
      });
      return {
        status: 'success',
        images,
        productPrice,
        productName,
        productDescription,
        errors: null
      };
    })
    .catch(function(e) {
      console.log(e);
      return {
        status: 'error',
        images,
        productPrice,
        productName,
        productDescription,
        errors: e
      };
    });
};
module.exports = {
  accessThisUrl,
  scraperThisUrl
};
