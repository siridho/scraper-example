const models = require('../models');
const Joi = require('joi');
const _ = require('lodash');
const cheerio = require('cheerio');
const axios = require('axios');
const app = (module.exports = require('express')());
const { scraperThisUrl } = require('../../app/helpers/general');
require('dotenv').config();

const linkList = async (req, res) => {
  try {
    const linkList = await models.Link.findAll();
    return res.status(200).json({
      code: 200,
      status: 'success',
      message: 'product list',
      data: linkList
    });
  } catch (e) {
    return res.status(400).json({
      code: 400,
      status: 'error',
      message: 'product list',
      data: linkList
    });
  }
};

const linkDetail = async (req, res) => {
  const { linkId } = req.params;
  const link = await models.Link.findOne({
    where: {
      id: linkId
    },
    include: {
      model: models.ProductImage,
      as: 'images',
      attributes: ['imageUrl']
    }
  });

  if (link === null) {
    return res.status(400).json({
      code: 400,
      status: 'error',
      message: 'data not found'
    });
  }
  return res.status(200).json({
    code: 200,
    status: 'success',
    message: 'detail product',
    data: link
  });
};

const linkCreate = async (req, res) => {
  const joiValidation = {
    link: Joi.string().required()
  };
  await Joi.validate(req.body, joiValidation, (err, value) => {
    if (err) {
      res.render('create', {
        status: 'error',
        message: 'Invalid request data',
        errors: [err.details[0].message]
      });
    }
  });
  let newLink;
  let transaction;
  try {
    let { link } = req.body;
    const {
      productDescription,
      productName,
      productPrice,
      images,
      errors
    } = await scraperThisUrl({ link });

    if (errors)
      return res.status(400).json({
        status: 'error',
        message: 'Invalid request data',
        errors: err
      });
    transaction = await models.sequelize.transaction();
    newLink = await models.Link.findOne({ where: { link }, transaction });

    if (!newLink) {
      newLink = await models.Link.create(
        {
          link,
          productDescription,
          productName,
          productPrice
        },
        { transaction }
      );
    } else {
      await newLink.update(
        { productDescription, productName, productPrice },
        { transaction }
      );
    }
    if (images.length) {
      await models.ProductImage.destroy(
        { where: { linkId: newLink.id } },
        { transaction }
      );
      for (i of images) {
        await models.ProductImage.create(
          { linkId: newLink.id, imageUrl: i },
          { transaction }
        );
      }
    }
    newLink = await models.Link.findOne({
      where: {
        id: newLink.id
      },
      include: {
        model: models.ProductImage,
        as: 'images',
        attributes: ['imageUrl']
      },
      transaction
    });
    await transaction.commit();
  } catch (err) {
    if (transaction) await transaction.rollback();

    return res.status(400).json({
      status: 'error',
      message: 'Invalid request data',
      errors: err
    });
  }
  return res.status(200).json({
    code: 200,
    status: 'success',
    message: 'detail product',
    data: newLink
  });
};

module.exports = {
  linkList,
  linkDetail,
  linkCreate
};
