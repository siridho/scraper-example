const Joi = require('joi');
const _ = require('lodash');

const { accessThisUrl } = require('../../app/helpers/general');
require('dotenv').config();
const port = process.env.PORT || 3000;
const baseUrl = `http://localhost:${port}/api`;
const linkDetailView = async (req, res) => {
  const { linkId } = req.params;
  const result = await accessThisUrl({
    method: 'get',
    url: `${baseUrl}/link/${linkId}`
  });
  const { data, errors, status } = result;

  return res.render('detail', {
    title: 'Product Detail',
    status,
    message: 'detail user',
    link: data,
    errors
  });
};
const linkCreateView = async (req, res) => {
  res.render('create', {
    title: 'register new link'
  });
};
const linkListView = async (req, res) => {
  const result = await accessThisUrl({
    method: 'get',
    url: `${baseUrl}/link`
  });
  const { data, errors, status } = result;
  res.render('list', {
    title: 'list of registered products',
    linkList: data,
    errors,
    status
  });
};
const linkValidateCreate = async (req, res) => {
  const joiValidation = {
    link: Joi.string().required()
  };
  await Joi.validate(req.body, joiValidation, (err, value) => {
    if (err) {
      res.render('create', {
        status: 'error',
        message: 'Invalid request data',
        errors: [err.details[0].message]
      });
    }
  });
  const { link } = req.body;
  const result = await accessThisUrl({
    method: 'post',
    url: `${baseUrl}/link/create`,
    data: {
      link
    }
  });
  const { data, errors, status } = result;
  if (errors) {
    res.render('create', {
      status: 'error',
      message: 'Invalid request data',
      errors: err
    });
  } else {
    return res.redirect('/link');
  }
};
module.exports = {
  linkDetailView,
  linkValidateCreate,
  linkCreateView,
  linkListView
};
