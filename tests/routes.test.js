const request = require('supertest');
const app = require('../');
jest.useFakeTimers();
describe('Create Link', () => {
  it('should create a new link', async () => {
    const res = await await request(app)
      .post('/api/link/create')
      .send({
        link: 'https://fabelio.com/ip/karpet-sweden.html'
      });
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty('data');
  });
  it('should fetch a single link', async () => {
    const linkId = 1;
    const res = await request(app).get(`/api/link/${linkId}`);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty('data');
  });
  it('should get product list', async () => {
    const res = await request(app).get(`/api/link/`);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty('data');
  });
});
