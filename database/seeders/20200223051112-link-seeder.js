'use strict';

module.exports = {
  up: async queryInterface => {
    const link = await queryInterface.bulkInsert(
      'links',
      [
        {
          id: 1,
          link: 'https://fabelio.com/ip/keset-mat-grey.html',
          product_name: 'Keset Lukas (Grey)',
          product_price: 99000,
          product_description:
            'Si Halus Lembut Pelengkap Dekorasi Ruang\n\nHalo, aku keset Lukas! Aku si karpet bulu dengan desain monokrom yang memberikan kesan maskulin pada ruangan. Karena material polyesterku yang sangat halus dan lembut, aku cocok sekali diletakkan di kamar tidur ataupun ruang keluarga. Meskipun begitu, aku cukup mudah dibersihkan, kok. Kamu hanya perlu membersihkanku dengan vacuum cleaner saja.',
          created_at: new Date(),
          updated_at: new Date()
        }
      ],
      {}
    );
    await queryInterface.bulkInsert(
      'product_images',
      [
        {
          link_id: 1,
          image_url:
            'https://m2fabelio.imgix.net/catalog/product/cache/image/700x350/e9c3970ab036de70892d86c6d221abfe/1/8/1803-a8-1_1_3.jpg'
        },
        {
          link_id: 1,
          image_url:
            'https://m2fabelio.imgix.net/catalog/product/cache/image/700x350/e9c3970ab036de70892d86c6d221abfe/1/8/1803-a8-3_3.jpg'
        },
        {
          link_id: 1,
          image_url:
            'https://m2fabelio.imgix.net/catalog/product/cache/image/700x350/e9c3970ab036de70892d86c6d221abfe/1/8/1803-a8-4_3.jpg'
        },
        {
          link_id: 1,
          image_url:
            'https://m2fabelio.imgix.net/catalog/product/cache/image/700x350/e9c3970ab036de70892d86c6d221abfe/l/u/lukas-1803-a8-2__3.jpg'
        }
      ],
      {}
    );
    return true;
  },

  down: queryInterface => {
    return Promise.resolve(true);
  }
};
